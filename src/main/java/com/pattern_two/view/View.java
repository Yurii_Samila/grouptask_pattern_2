package com.pattern_two.view;

import com.pattern_two.model.Developer;
import com.pattern_two.model.Task;
import com.pattern_two.model.Team;

import java.util.List;

public class View {

    public void printMenu(){
        System.out.println("Choose user");
    }

    public void printDevelopers(Team team){
        List<Developer> developers = team.getDevelopers();
        for (int i = 0; i < developers.size(); i++) {
            System.out.println((i + 1) + " " + developers.get(i).getName());
        }
        System.out.println((developers.size() + 1) +" " + team.getAdmin().getName() + "(Administrator)");
    }

    public void printAdminMethods(){
        System.out.println("1) Add new task in ToDo\n" +
                "2) Return task in ToDo\n" +
                "3) Add task in Done\n" +
                "4) Add task in Blocked\n" +
                "0) Quit");
    }
    public void printNewTask(){
        System.out.println("Please, tap new task");
    }

    public void printTasks(List<Task> tasks){
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i+1) + " " + tasks.get(i));

        }
    }
    public void printDeveloperMethods(){
        System.out.println("1) Add task in Progress\n"
            + "2) Add task in Code Review\n"
            + "3) Add task in Test\n"
            + "4) Add task in Verification");
    }

    public void chooseTaskForAddingInProgress(){
        System.out.println("Choose task to add in progress");
    }

}
