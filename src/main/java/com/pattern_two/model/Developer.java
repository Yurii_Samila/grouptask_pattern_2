package com.pattern_two.model;

import java.util.ArrayList;
import java.util.List;

public class Developer {

    private String name;
    private Task task;
    private List<Task> finishedTasks;

    public Developer(String name) {
        this.name = name;
        finishedTasks = new ArrayList<>();
    }

    public void addInProgress(Task task){
    if (this.task!=null){
        System.out.println("You already have a task!!!!");
        return;
    }
    this.task = task;
    task.setTaskDeveloper(this);
    task.addInProgress();
}

    public void addInCodeReview(){
        if (task!= null) {
            task.addInCodeReview();
        }else {
            System.out.println("You don't have task");
        }
}

    public void addInTest(){
        if (task!= null) {
            task.addInTest();
        }else {
            System.out.println("You don't have task");
        }
    }

    public void addInVerification(){
        if (task!= null) {
            task.addInVerification();
        }else {
            System.out.println("You don't have task");
        }
    }

    public String getName() {
        return name;
    }

//    public Task getTask() {
//        return task;
//    }

    public void setTask(Task task) {
        this.task = task;
    }

    public void setFinishedTasks(List<Task> finishedTasks) {
        this.finishedTasks = finishedTasks;
    }

    public List<Task> getFinishedTasks() {
        return finishedTasks;
    }


}
