package com.pattern_two.model;


import com.pattern_two.model.state_impl.ToDoState;

public class Task {
    private static int unique = 1;
    private int taskId;
    private String taskName;
    private State taskState = new ToDoState();//TODO add ToDoState
    private String stateName;
    private Developer taskDeveloper;

    public Task(String taskName) {
        this.taskId = unique;
        this.taskName = taskName;
        this.taskDeveloper = null;
        this.stateName = this.taskState.getStateName();
        unique++;
    }

    public void addInProgress() {
        taskState.addInProgress(this);
    }

    public void addInToDo(){
        taskState.addInToDo(this);
    }

    public void addInCodeReview(){
        taskState.addInCodeReview(this);
    }

    public void addInTest(){
        taskState.addInTest(this);
    }

    public void addInDone(){
        taskState.addInDone(this);
    }

    public void addInVerification(){
        taskState.addInVerification(this);
    }

    public void addInBlocked(){
        taskState.addInBlocked(this);
    }

    public int getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public State getTaskState() {
        return taskState;
    }

    public void setTaskState(State taskState) {
        this.taskState = taskState;
    }

    public Developer getTaskDeveloper() {
        return taskDeveloper;
    }

    public void setTaskDeveloper(Developer taskDeveloper) {
        this.taskDeveloper = taskDeveloper;
    }

    @Override
    public String toString() {
        return "Task{" +
            "taskId=" + taskId +
            ", taskName='" + taskName + '\'' +
            ", taskState=" + taskState +
            ", taskDeveloper=" + taskDeveloper +
            '}';
    }
}
