package com.pattern_two.model.state_impl;

import com.pattern_two.model.State;


public class BlockedState implements State {

    @Override
    public String getStateName() {
        return "BlockedState";
    }

}

