package com.pattern_two.controller;

import com.pattern_two.model.Admin;
import com.pattern_two.model.Developer;
import com.pattern_two.model.Task;
import com.pattern_two.model.TasksHolder;
import com.pattern_two.model.Team;
import com.pattern_two.model.state_impl.CodeReviewState;
import com.pattern_two.model.state_impl.ProgressState;
import com.pattern_two.model.state_impl.TestState;
import com.pattern_two.model.state_impl.VerificationState;
import com.pattern_two.view.View;

import java.util.List;
import java.util.Scanner;

public class MainController {

    private TasksHolder tasksHolder;
    private Team team;
    private Scanner sc = new Scanner(System.in);
    private View view = new View();


    public MainController() {
        tasksHolder = new TasksHolder();
        team = new Team(tasksHolder);
    }

    public void run() {
    }

    public void menu() {
        List<Task> tasks = tasksHolder.getTasks();
//        view.printMenu();
//        view.printDevelopers(team);
//        int choice = sc.nextInt();
        while (true) {
            view.printMenu();
            view.printDevelopers(team);
            int choice = sc.nextInt();
            switch (choice) {
                case 5:
                    view.printAdminMethods();
                    int choice2 = sc.nextInt();
                    Admin admin = team.getAdmin();
                    while (choice2 != 0) {
                        switch (choice2) {
                            case 1:
                                view.printNewTask();
                                sc.nextLine();//for not getting empty line
                                String taskName = sc.nextLine();
                                Task task = new Task(taskName);
                                admin.addInToDo(task);
                                view.printAdminMethods();
                                choice2 = sc.nextInt();
                                break;
                            case 2:
                                view.printTasks(tasks);
                                int taskIndex = sc.nextInt() - 1;
                                admin.returnInToDo(tasks.get(taskIndex));
                                view.printAdminMethods();
                                choice2 = sc.nextInt();
                                break;
                            case 3:
                                view.printTasks(tasks);
                                taskIndex = sc.nextInt() - 1;
                                admin.addInDone(tasks.get(taskIndex));
                                view.printAdminMethods();
                                choice2 = sc.nextInt();
                                break;
                            case 4:
                                view.printTasks(tasks);
                                taskIndex = sc.nextInt() - 1;
                                admin.addInBlocked(tasks.get(taskIndex));
                                view.printAdminMethods();
                                choice2 = sc.nextInt();
                                break;
                        }
                    }
                    break;
                case 1:
                case 2:
                case 3:
                case 4: {
                    int developerIndex = choice;
                    Developer currentDeveloper = team.getDevelopers().get(developerIndex);
                    view.printDeveloperMethods();
                    int methodIndex = sc.nextInt();
                    switch (methodIndex) {
                        case 1:
                            view.chooseTaskForAddingInProgress();
                            view.printTasks(tasks);
                            int taskIndex = sc.nextInt() - 1;
                            currentDeveloper.addInProgress(tasks.get(taskIndex));
                            break;
                        case 2:
                            currentDeveloper.addInCodeReview();
                            break;
                        case 3:
                            currentDeveloper.addInTest();
                            break;
                        case 4:
                            currentDeveloper.addInVerification();
                            break;
                    }
                }
            }
        }
    }
//    private
}
